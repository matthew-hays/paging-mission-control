//*********************************************************************************
//  PAGING MISSION CONTROL SUBMISSION
//  https://gitlab.com/enlighten-challenge/paging-mission-control
//  -------------------------------------------------------------------------------
//  PMCClass.java		                                       Author: Matthew Hays
//  The class for PMCDriver.java
//
//  References:
//  * https://docs.oracle.com/en/java/javase/14/language/records.html
//  * https://stackoverflow.com/a/3481842/19657086
//  * https://stackoverflow.com/a/17249568/19657086
//  * https://stackoverflow.com/a/22463063/19657086
//  * https://www.digitalocean.com/community/tutorials/java-convert-string-to-double
//  * https://blog.hubspot.com/website/java-map
//  * https://stackabuse.com/java-how-to-get-keys-and-values-from-a-map/
//  * https://howtodoinjava.com/java/date-time/compare-localdatetime/
//  * https://stackoverflow.com/a/46943265/19657086
//**********************************************************************************

package PMC;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PMCClass {	
	final class Status {
		final LocalDateTime ts;
		final String sid, c;
		final int rhl, rll;
		final double rv;
		
		public Status(LocalDateTime ts, String sid, int rhl, int rll, double rv, String c) {
	        this.ts = ts;
	        this.sid = sid;
	        this.rhl = rhl;
	        this.rll = rll;
	        this.rv = rv;
	        this.c = c;
	    }
		
		LocalDateTime ts() { return ts; }
	    String sid() { return sid; }
		int rhl() { return rhl; }
	    int rll() { return rll; }
		double rv() { return rv; }
	    String c() { return c; }
	    
	    @Override public String toString() {
			// Default: return getClass().getName()+"@"+Integer.toHexString(hashCode());
	        return "timestamp: " + ts + ", satelliteID: " + sid + ", red-high-limit: " + rhl + ", red-low-limit: " + rll + ", raw-value: " + rv;
	    }
	}
	
	// Global Variables used from start to finish
	Map<String, List<Status>> statusMap = new HashMap<String, List<Status>>();
	int alertCount = 0;
	
	// Global Variables assigned per Status
	LocalDateTime timestamp;
	String satelliteID, component;
	int rHighLimit, rLowLimit;
	double rawValue;
	
	public void ingestStatus(String data) {
		String[] statusData = data.split("\\|");
		
		String strTimestamp = statusData[0];	// <timestamp>
		satelliteID = statusData[1];			// <satellite-id>
		String strRHighLimit = statusData[2];	// <red-high-limit> (<yellow-high-limit> not needed)
		String strRLowLimit = statusData[5];	// <red-low-limit> (<yellow-low-limit> not needed)
		String strRawValue = statusData[6];		// <raw-value>
		component = statusData[7];				// <component>
		
		stringToTimestamp(strTimestamp);
		rHighLimit = stringToInt(strRHighLimit);
		rLowLimit = stringToInt(strRLowLimit);
		rawValue = stringToDouble(strRawValue);
		Status status = new Status(timestamp, satelliteID, rHighLimit, rLowLimit, rawValue, component);
		
		if (!statusMap.containsKey(satelliteID)) {
			List<Status> newStatusList = new ArrayList<Status>();
			statusMap.put(satelliteID, newStatusList);
		}
		statusMap.get(satelliteID).add(status);
	}

	public void stringToTimestamp(String str) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("uuuuMMdd HH:mm:ss.SSS");
		timestamp = LocalDateTime.parse(str, formatter);
	}

	public int stringToInt(String str) {
		int i = Integer.parseInt(str);
		return i;
	}
	
	public double stringToDouble(String str) {
		double d = Double.parseDouble(str);
		return d;
	}

	public void iterateStatusMap() {
		for (Map.Entry<String, List<Status>> pair : statusMap.entrySet()) { // For Loop for every Key (Satellite ID) in the Map
			List<Status> rHighLimitList = new ArrayList<Status>();
			List<Status> rLLimitList = new ArrayList<Status>();
			
			for (int i = 0; i < pair.getValue().size(); i++) { // For Loop for every List in Key
				Status statusData = pair.getValue().get(i);
				timestamp = pair.getValue().get(i).ts();
				satelliteID = pair.getValue().get(i).sid();
				rHighLimit = pair.getValue().get(i).rhl();
				rLowLimit = pair.getValue().get(i).rll();
				rawValue = pair.getValue().get(i).rv();
				component = pair.getValue().get(i).c();
				
				rHighLimitTest(statusData, rHighLimitList);
				rLowLimitTest(statusData, rLLimitList);
			}
			
			if (rHighLimitList.size() >= 3)
				rHighLimitAlert(rHighLimitList);
			
			if (rLLimitList.size() >= 3)
				rLowLimitAlert(rLLimitList);
		}
	}
	
	public void rHighLimitTest(Status sd, List<Status> rHLimitList) {
		rawValue = sd.rv();
		rHighLimit = sd.rhl();
		component = sd.c();
		
		if (rawValue > rHighLimit && component.equals("TSTAT"))
			rHLimitList.add(sd);
	}
	
	public void rLowLimitTest(Status sd, List<Status> rLLimitList) {
		rawValue = sd.rv();
		rLowLimit = sd.rll();
		component = sd.c();
		
		if (rawValue < rLowLimit && component.equals("BATT"))
			rLLimitList.add(sd);
	}
	
	public void rHighLimitAlert(List<Status> rHLimitList) {
		for (int i = 0; i < rHLimitList.size() - 2; i++) {
			int j = i + 2;
			LocalDateTime timeA = rHLimitList.get(i).ts();
			LocalDateTime timeB = rHLimitList.get(j).ts();
			long diff = Math.abs(ChronoUnit.MINUTES.between(timeA, timeB)); // 5 Minute difference between the two times
			
			if (diff <= 5) { // 5 being "5 Minutes"
				satelliteID = rHLimitList.get(i).sid();
				component = rHLimitList.get(i).c();
				if (alertCount > 0)
					System.out.println(",");
				
				System.out.println("    {");
				System.out.println("        \"satelliteId\": " + satelliteID + ",");
				System.out.println("        \"severity\": \"RED HIGH\",");
				System.out.println("        \"component\": \"" + component + "\",");
				System.out.println("        \"timestamp\": " + timeA + "Z");
				System.out.print("    }");
				alertCount++;
			}
		}
	}
	
	public void rLowLimitAlert(List<Status> rLLimitList) {
		for (int i = 0; i < rLLimitList.size() - 2; i++) {
			int j = i + 2;
			LocalDateTime timeA = rLLimitList.get(i).ts();
			LocalDateTime timeB = rLLimitList.get(j).ts();
			long diff = Math.abs(ChronoUnit.MINUTES.between(timeA, timeB)); // 5 Minute difference between the two times
						
			if (diff <= 5) { // 5 being "5 Minutes"
				satelliteID = rLLimitList.get(i).sid();
				component = rLLimitList.get(i).c();
				if (alertCount > 0)
					System.out.println(",");
				
				System.out.println("    {");
				System.out.println("        \"satelliteId\": " + satelliteID + ",");
				System.out.println("        \"severity\": \"RED LOW\",");
				System.out.println("        \"component\": \"" + component + "\",");
				System.out.println("        \"timestamp\": " + timeA + "Z");
				System.out.print("    }");
				alertCount++;
			}
		}
	}
}