//***********************************************************************************
//  PAGING MISSION CONTROL SUBMISSION
//  https://gitlab.com/enlighten-challenge/paging-mission-control
//  ---------------------------------------------------------------------------------
//  PMCDriver.java		                                         Author: Matthew Hays
//  The driver for PMCClass.java
//
//  References:
//  * https://www.digitalocean.com/community/tutorials/java-read-file-line-by-line
//  * Reference: https://www.w3schools.com/java/showjava.asp?filename=demo_api_scanner
//  * Reference: https://chortle.ccsu.edu/java5/notes/chap23/ch23_14.html
//************************************************************************************

package PMC;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class PMCDriver {
	public static void main(String[] args) {
		BufferedReader reader;
		PMCClass pmcClass = new PMCClass();

		try {
			Scanner scanner = new Scanner(System.in);

			System.out.println("SATELLITE RESPONSE SYSTEM ACTIVATED!\n");
			System.out.println("Enter the following:");
			System.out.println("1. The full filepath of an ASCII file that includes valid Ingest status telemetry data as pipe delimited records in the following format:");
			System.out.println("<timestamp>|<satellite-id>|<red-high-limit>|<yellow-high-limit>|<yellow-low-limit>|<red-low-limit>|<raw-value>|<component>");
			System.out.println("2. <timestamp>s are in Descending Order (from earliest to latest).");
			System.out.println("\nOne or more Alert Messages, in JSON format, will generate if:");
			System.out.println("1. If for the same satellite there are three battery voltage readings that are under the red low limit within a five minute interval.");
			System.out.println("2. If for the same satellite there are three thermostat readings that exceed the red high limit within a five minute interval.");
			System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------");
			String filepath = scanner.nextLine();
			scanner.close();
			System.out.println("");
			
			reader = new BufferedReader(new FileReader(filepath));
			String line = reader.readLine();

			while (line != null) {
				pmcClass.ingestStatus(line);
				// read next line
				line = reader.readLine();
			}
			reader.close();
			
			System.out.println("[");
			pmcClass.iterateStatusMap();
			System.out.println("\n]");
		} catch (IOException e) {
			System.out.println("Input File not Found.");
			e.printStackTrace();
		}
	}
}