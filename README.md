Paging Mission Control Submission
------------
This is a Project Entry being submitted to the [Paging Mission Control](https://gitlab.com/enlighten-challenge/paging-mission-control) repository.

This Project Entry was programmed in **Java 17** in the *Eclipe* IDE Framework (`Version: 2022-12 (4.26.0), Build id: 20221201-1913`) and built/installed with Maven `apache-maven-3.9.0`.

This Project Entry was programmed and submitted by **Matthew Hays**.

How to Run the Java Code
----------------
Git Clone the project into a Java IDE Framework `workspace` directory.

Then open and run the file `src/PMC/PMCDriver.java`.

`JavaSE-17` is the recommended JRE. Compiler compliance level is set to `17`.

When running the program, you'll be asked for the full filepath of a valid file (details in the program). Input the full filepath for `input/data1.txt`.

How to Maven Test
------------------
Right-click the Project -> `Run As` -> `Maven Test` should build successfully.